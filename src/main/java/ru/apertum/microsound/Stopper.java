/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * @author Evgeniy Egorov
 */
public class Stopper {

    public static void goStopper() {
        final Thread stopper = new Thread(() -> {
            try {
                final ServerSocket stopkServ = new ServerSocket(38008);
                stopkServ.setSoTimeout(60000);
                System.out.println("Stopping thread starting...");
                while (true) {
                    try {
                        final Socket socket = stopkServ.accept();
                        final InputStream is = socket.getInputStream();
                        while (is.available() == 0) {
                            Thread.sleep(100);
                        }
                        final byte[] result = new byte[is.available()];
                        is.read(result);
                        if (result.length == 3 && result[0] == 0x01 && result[1] == 0x02 && result[2] == 0x03) {
                            System.out.println("Server is stopping by command.");
                            System.exit(0);
                        }
                    } catch (SocketTimeoutException e) {
                        // so fucking what...
                    } catch (IOException | InterruptedException e) {
                        System.err.println("Stopping socket problems." + e);
                    }
                }// while
            } catch (Exception ex) {
                System.err.println("Stopping problems. " + ex);
            }
        });
        stopper.setDaemon(false);
        stopper.start();
    }
}
