/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

/**
 * Серверное приложения передачи звука через один центр
 *
 * @author Evgeniy Egorov
 */
public class Transit {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("go->");
        Microsound.getInstance().startTransitServer(Params.get().editServerPort);
        Stopper.goStopper();
    }

}
