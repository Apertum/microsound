/*
 * Run.java
 */
package ru.apertum.microsound;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/**
 * Отладка
 * The main class of the application.
 */
public class Run {

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {

        boolean wait = true;

        // ---------------------
        // Ping request sending.
        // ---------------------
        // Creating and binding the socket.
        final DatagramSocket socket;// < - связующее звено для пиема-передачи
        try {
            //System.err.println("creating and binding socket...");
            socket = new DatagramSocket();
        } catch (SocketException exception) {
            // An unknown / unexpected network error was encountered. (Not a normal situation.)
            throw new RuntimeException("3. An unknown / unexpected network error was encountered. (Not a normal situation.)", exception);
        }

        // Sending the datagram.
        new Thread(() -> {
            try {
                byte[] to = Microsound.longToBytes(22222L);
                byte[] from = Microsound.longToBytes(11111L);
                byte[] datagramBuffer = "--------------------123456789012345678901234567890123456789012345678901234567890".getBytes();
             //   byte[] datagramBuffer = "--------------------qwertyuiopasdfghjklzxcvbnmmnbvcxzlkjghfdsapoiuytttrewq".getBytes();
             //   byte[] to = Microsound.longToBytes(11111L);
             //   byte[] from = Microsound.longToBytes(22222L);
                byte[] dd = new byte[12];
                dd[0] = 0x13;
                dd[1] = 0x13;
                dd[2] = 0x13;
                dd[3] = 0x13;
                datagramBuffer[0] = 0x13;
                datagramBuffer[1] = 0x13;
                datagramBuffer[2] = 0x13;
                datagramBuffer[3] = 0x13;
                System.arraycopy(from, 0, dd, 4, 8);
                System.arraycopy(from, 0, datagramBuffer, 4, 8);
                System.arraycopy(to, 0, datagramBuffer, 4 + 8, 8);

               
                // Creating the datagram destination (that is the server socket address).
                final InetSocketAddress datagramDestination = new InetSocketAddress("127.0.0.1", 11111);
                socket.send(new DatagramPacket(dd, 0, dd.length, datagramDestination));

                //System.err.println("sending datagram...");
                for (int i = 0; i < 1000; i++) {

                    int datagramBufferOffset = 0;

                    // Creating the datagram.
                    DatagramPacket datagram;
                    try {
                        //System.err.println("creating datagram...");
                        datagram = new DatagramPacket(datagramBuffer, datagramBufferOffset, datagramBuffer.length > 1024 + 16 + 4 ? 1024 + 16 + 4 : datagramBuffer.length, datagramDestination);
                    } catch (Exception exception) {
                        // An unknown / unexpected network error was encountered. (Not a normal situation.)
                        throw new RuntimeException("4. An unknown / unexpected network error was encountered. (Not a normal situation.)", exception);
                    }

                    socket.send(datagram);
                    System.out.println("         " + new String(datagramBuffer) + " -->");

                    Thread.sleep(1000);

                }

            } catch (IOException exception) {
                // The datagram could not be sent. (Maybe destination is unreachable.)
                throw new RuntimeException("The datagram could not be sent. (Maybe destination is unreachable.). ", exception);
            } catch (InterruptedException ex) {
                System.err.println(ex);
            }
        }).start();

        while (!Thread.interrupted()) {
            

            final String message;
            if (wait) {

                // ----------
                // This is the pong reply receiving.
                // ----------
                // Set a maximum datagram backing buffer size.
                int datagramBufferSize = 2048;

                // Create the datagram backing buffer.
                byte[] datagramBuffer = new byte[datagramBufferSize];
                int datagramBufferOffset = 0;

                // Re-create the datagram.
                DatagramPacket datagram = new DatagramPacket(datagramBuffer, datagramBufferOffset, datagramBufferSize);

                boolean flag = true;
                // Receiving the datagram.
                try {
                    //System.err.println("receiving datagram...");
                    socket.setSoTimeout(5000);
                    socket.receive(datagram);
                } catch (SocketTimeoutException exception) {
                    flag = false;
                } catch (IOException exception) {
                    // An unknown / unexpected network error was encountered. (Not a normal situation.)
                    throw new RuntimeException("5. An unknown / unexpected network error was encountered. (Not a normal situation.) ", exception);
                }
                if (flag) {
                    // Obtaining the datagram actual length (how much data was received, or how much of the backing buffer was used).
                    int datagramBufferUsed = datagram.getLength();

                    // Decoding the message (which should be a string).
                    message = new String(datagramBuffer, datagramBufferOffset, datagramBufferUsed);

                    //System.err.println("received -->");
                    //System.err.println("    message = " + message);
                } else {
                    message = null;
                }
            } else {
                message = null;
            }

            System.out.println("<-- MAS: " + message);
        }
        // ----------
        // Closing the socket.
        //System.err.println("closing socket...");
        socket.close();

        if (1 == 1) {
            return;
        }

        int port = 21004;

        Microsound.getInstance().startSoundServer(port, null, 0 , null);

        Microsound.getInstance().open(null, new File("e:\\microsound.wav"), InetAddress.getByName("127.0.0.1"), port);
        for (int i = 0; i < 30; i++) {
            System.out.println("### itr=" + i);
            Thread.sleep(1000);
        }
        System.out.println("### itr END");
        Microsound.getInstance().stop();
        Thread.sleep(2000);
        if (1 == 1) {
            return;
        }

        /*
         java.awt.EventQueue.invokeLater(new Runnable() {
         public void run() {
         FMicro dialog = new FMicro(new javax.swing.JFrame(), true);
         dialog.addWindowListener(new java.awt.event.WindowAdapter() {
         @Override
         public void windowClosing(java.awt.event.WindowEvent e) {
         System.exit(0);
         }
         });
         dialog.setVisible(true);
         }
         });
         */
        Thread th = new Thread(new Runnable() {
            private DatagramSocket socket;

            public void run() {

                // Obtain and open the line.
                SourceDataLine sLine;
                try {
                    sLine = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, getAudioFormat()));
                    sLine.open(getAudioFormat());
                } catch (LineUnavailableException ex) {
                    System.err.println("No S LINE");
                    throw new RuntimeException(ex);
                }

                // read chunks from a stream and write them to a source data 
                sLine.start();

                byte[] buffer = new byte[41100];

                int port = 27001;
                try {
                    socket = new DatagramSocket(port);
                } catch (SocketException ex) {
                    throw new RuntimeException("Невозможно создать UDP-сокет на порту " + port + ". " + ex.getMessage());
                }
                while (true) {
                    //Receive request from client
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    try {
                        socket.receive(packet);
                    } catch (IOException ex) {
                        if (!Thread.interrupted()) {
                            throw new RuntimeException("Невозможно принять UDP-сообщение. " + ex.getMessage());
                        }
                    }
                    InetAddress client = packet.getAddress();
                    int client_port = packet.getPort();
                    //final String message = new String(buffer, packet.getOffset(), packet.getLength());
                    System.out.println("### ----------------------------------------------------------------------get=" + packet.getLength());

                    sLine.write(buffer, 0, packet.getLength());
                }
            }
        });
        th.setDaemon(true);
        th.start();

        open(getAudioFormat());
    }

    private static AudioFormat getAudioFormat() {
        float sampleRate = 41100.0F;
        //8000,11025,16000,22050,44100
        int sampleSizeInBits = 16;
        //8,16
        int channels = 1;
        //1,2
        boolean signed = true;
        //true,false
        boolean bigEndian = false;
        //true,false
        return new AudioFormat(sampleRate,
                sampleSizeInBits,
                channels,
                signed,
                bigEndian);
    }//end getAudioFormat

    public static void open(AudioFormat format) throws FileNotFoundException, IOException, InterruptedException {
        TargetDataLine line = null;
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format); // format is an AudioFormat object
        if (!AudioSystem.isLineSupported(info)) {
            // Handle the error ... 
            System.err.println("Audio System is NOT Line Supported");
            throw new RuntimeException("Audio System is NOT Line Supported");
        }
        // Obtain and open the line.
        try {
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(format);
        } catch (LineUnavailableException ex) {
            System.err.println("No LINE");
            throw new RuntimeException(ex);
        }

        // Assume that the TargetDataLine, line, has already
        // been obtained and opened.
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //FileOutputStream out = new FileOutputStream("e:\\microsound.wav");
        int numBytesRead;
        System.out.println(line.getBufferSize());
        byte[] data = new byte[line.getBufferSize()];

        // Begin audio capture.
        line.start();

        int i = 0;
        long t = System.currentTimeMillis();
        // Here, stopped is a global boolean set by another thread.
        while (!stopped) {
            stopped = (System.currentTimeMillis() - t) > 9000;
            if (stopped) {
                line.stop();
                line.close();
                System.out.println("END");
                //Thread.sleep(1000);

            } else {
                System.out.println("*** itr " + i++);
            }
            // Read the next chunk of data from the TargetDataLine.
            System.out.println(data.length);
            numBytesRead = line.read(data, 0, data.length);
            // Save this chunk of data.
            out.write(data, 0, numBytesRead);

            if (numBytesRead != 0) {
                sendUDPMessage(data, InetAddress.getByName("127.0.0.1"), 27001);
            }

        }
        out.flush();
        out.close();

        //  line.stop();
        //  line.close();
        //  out.flush();
        AudioFileFormat.Type fileType = null;
        File audioFile = null;
        fileType = AudioFileFormat.Type.WAVE;
        audioFile = new File("e:\\microsound.wav");

        byte[] bb = out.toByteArray();
        ByteArrayInputStream in = new ByteArrayInputStream(bb);
        AudioSystem.write(
                new AudioInputStream(in, format, bb.length),
                fileType,
                audioFile);

        in.close();

        SourceDataLine sLine;

        // Obtain and open the line.
        try {
            sLine = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, format));
            sLine.open(format);
        } catch (LineUnavailableException ex) {
            System.err.println("No S LINE");
            throw new RuntimeException(ex);
        }

        // read chunks from a stream and write them to a source data 
        sLine.start();

        //   sLine.write(bb, 0, bb.length);
        //sLine.write(bb, 0, bb.length);
        Thread.sleep(2000);

    }

    public static boolean stopped = false;

    public static void sendUDPMessage(byte mess_b[], InetAddress address, int port) {
        final DatagramSocket socket;
        final DatagramPacket packet = new DatagramPacket(mess_b, mess_b.length, address, port);
        try {
            socket = new DatagramSocket();
        } catch (SocketException ex) {
            throw new RuntimeException("Проблемы с сокетом UDP." + ex.getMessage());
        }
        try {
            socket.send(packet);
        } catch (IOException io) {
            throw new RuntimeException("Ошибка отправки сообщения по UDP. " + io.getMessage());
        } finally {
            socket.close();
        }
    }
}
