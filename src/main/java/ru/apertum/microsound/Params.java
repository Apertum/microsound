/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Properties;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Evgeniy Egorov
 */
public final class Params {

    public final static String FILE_PROPS = "config/microsound.properties";

    private Params() {
        load();
    }

    public static Params get() {
        return ParamsHolder.INSTANCE;
    }

    private static class ParamsHolder {

        private static final Params INSTANCE = new Params();
    }

    public final LinkedList<Partner> list = new LinkedList<>();
    public boolean cbReceiveMess = false;
    public boolean cbUseServer = false;
    public boolean cbNotify = false;
    public String editServerAddress = null;
    public int editServerPort = 0;
    public long editID = 11012405413L;
    public int editLocalPort = 0;
    public Locale curerntLocale = Locale.UK;

    /**
     * @return is new
     */
    public boolean load() {
        File f = new File("config");
        if (!f.exists()) {
            f.mkdir();
        }

        f = new File(FILE_PROPS);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                System.err.println("2: " + ex);
            }
        }
        Properties p = new Properties();

        try {
            p.load(new FileInputStream(f));
        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex);
        }

        cbReceiveMess = ("1".equals(p.getProperty("receive_message", "0")));
        cbUseServer = ("1".equals(p.getProperty("use_central_server", "0")));

        editServerAddress = (p.getProperty("server_address", "127.0.0.1"));
        editServerPort = Integer.parseInt(p.getProperty("server_port", "").isEmpty() ? "22001" : p.getProperty("server_port", "22001"));
        editID = Long.parseLong(p.getProperty("pid", "").isEmpty() ? ("" + System.currentTimeMillis()) : p.getProperty("pid", "" + System.currentTimeMillis()));
        cbNotify = ("1".equals(p.getProperty("notify_about_presence", "0")));
        editLocalPort = Integer.parseInt(p.getProperty("local_port", "22001").isEmpty() ? "22001" : p.getProperty("local_port", "22001"));

        list.clear();
        String[] ss = p.getProperty("partners", "").split("&");
        for (String s : ss) {
            if (!s.trim().isEmpty()) {
                String[] pt = s.split("\\.\\s");
                list.add(new Partner(s.substring(0, s.indexOf(pt[pt.length - 1]) - 2), pt[pt.length - 1].split(":")[0], Integer.parseInt(pt[pt.length - 1].split(":")[1])));
            }
        }
        final String lng = p.getProperty("lng", "");
        LOCALES.forEach(loc -> {
            if ((loc.getLanguage() + "_" + loc.getCountry()).equalsIgnoreCase(lng)) {
                curerntLocale = loc;
            }
        });

        return !p.containsKey("receive_message");
    }

    public void save() {
        File f = new File("config");
        if (!f.exists()) {
            f.mkdir();
        }

        f = new File(FILE_PROPS);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
        final Properties p = new Properties();
        p.setProperty("receive_message", cbReceiveMess ? "1" : "0");
        p.setProperty("use_central_server", cbUseServer ? "1" : "0");
        p.setProperty("server_address", editServerAddress.trim());
        p.setProperty("server_port", Integer.toString(editServerPort));
        p.setProperty("pid", Long.toString(editID));
        p.setProperty("notify_about_presence", cbNotify ? "1" : "0");
        p.setProperty("local_port", Integer.toString(editLocalPort));
        final StringBuilder sb = new StringBuilder();
        list.forEach(pr -> {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(pr);
        });
        p.setProperty("partners", sb.toString());
        sb.setLength(0);

        p.setProperty("lng", (curerntLocale.getLanguage() + "_" + curerntLocale.getCountry()));

        try {
            p.store(new FileOutputStream(f), "Microsound properties");
        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex);
        }
    }

    public static final ObservableList<Locale> LOCALES = FXCollections.observableArrayList(
            new Locale("ru", "RU"),
            Locale.UK,
            Locale.GERMANY,
            new Locale("it", "IT"),
            new Locale("es", "ES"),
            new Locale("pt", "PT"),
            new Locale("uk", "UA")
    );
}
