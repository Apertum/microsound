/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound.fx;

import javafx.scene.control.TextField;

/**
 * @author Evgeniy Egorov
 */
public class PortField extends TextField {

    private long limit = Long.MAX_VALUE;

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public PortField() {
        this.setPromptText("port");
    }

    public PortField(String text) {
        super(text);
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (validate(text)) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (validate(text)) {
            super.replaceSelection(text);
        }
    }

    private boolean validate(String text) {
        try {
            Long.parseLong(getText() + text);
        } catch (NumberFormatException ex) {
            return false;
        }
        return (text.isEmpty() || text.matches("[0-9]")) && Long.parseLong(getText() + text) <= getLimit();
    }

}
