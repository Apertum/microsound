/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/**
 * @author Evgeniy Egorov
 */
public class Microsound {

    final DatagramSocket socket;

    private Microsound() {
        try {
            socket = new DatagramSocket();
        } catch (SocketException ex) {
            throw new RuntimeException("Problem with socket UDP." + ex.getMessage());
        }
        audioFormat = getDefaultAudioFormat();
    }

    public static Microsound getInstance() {
        return MicrosoundHolder.INSTANCE;
    }

    private static class MicrosoundHolder {

        private static final Microsound INSTANCE = new Microsound();
    }

    public void sendUDPMessage(byte mess_b[], InetAddress address, int port) {
        sendUDPMessage(mess_b, address, port, null, null);
    }

    public void sendUDPMessage(byte mess_b[], InetAddress address, int port, Long from, Long to) {
        if (from == null || to == null) {

            final DatagramPacket packet = new DatagramPacket(mess_b, mess_b.length, address, port);
            try {
                socket.send(packet);
            } catch (IOException io) {
                throw new RuntimeException("No sent data by UDP. " + io.getMessage());
            } finally {
                //socket.close();
            }
        } else {

            byte[] dd = new byte[1024 + 16 + 4];
            dd[0] = 0x13;
            dd[1] = 0x13;
            dd[2] = 0x13;
            dd[3] = 0x13;

            System.arraycopy(longToBytes(from), 0, dd, 4, 8);
            System.arraycopy(longToBytes(to), 0, dd, 4 + 8, 8);

            int p = 0;
            while (p * 1024 < mess_b.length) {

                System.arraycopy(mess_b, p * 1024, dd, 4 + 8 + 8, mess_b.length - p * 1024 > 1024 ? 1024 : (mess_b.length - p * 1024));

                final DatagramPacket packet = new DatagramPacket(dd, dd.length, address, port);
                try {
                    socket.send(packet);
                } catch (IOException io) {
                    throw new RuntimeException("No sent data by UDP. " + io.getMessage());
                } finally {
                    //socket.close();
                }
                p++;
            }

        }
    }

    private AudioFormat audioFormat;

    public AudioFormat getAudioFormat() {
        return audioFormat;
    }

    public void setAudioFormat(AudioFormat audioFormat) {
        this.audioFormat = audioFormat;
    }

    private AudioFormat getDefaultAudioFormat() {
        float sampleRate = 41100.0F;
        //8000,11025,16000,22050,44100
        int sampleSizeInBits = 16;
        //8,16
        int channels = 1;
        //1,2
        boolean signed = true;
        //true,false
        boolean bigEndian = false;
        //true,false
        return new AudioFormat(sampleRate,
                sampleSizeInBits,
                channels,
                signed,
                bigEndian);
    }//end getAudioFormat

    private Thread soundServerThread;
    private Thread receiveThread;

    private void stopThread(Thread tr) {
        if (tr != null && tr.isAlive()) {
            tr.interrupt();
            int k = 0;
            while (tr.isAlive() && (k++) < 10) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    public void startSoundServer(final int port) {
        startSoundServer(port, null, 0, null);
    }

    public void startSoundServer(final InetAddress serverAddress, final int serverPort, Long pid) {
        startSoundServer(0, serverAddress, serverPort, pid);
    }

    public void startSoundServer(final int port, final InetAddress serverAddress, final int serverPort, Long pid) {
        stopThread(soundServerThread);
        stopThread(receiveThread);
        if (serverAddress != null && serverPort > 0 && pid != null) {
            receiveThread = new Thread(() -> {
                byte[] dd = new byte[12];
                dd[0] = 0x13;
                dd[1] = 0x13;
                dd[2] = 0x13;
                dd[3] = 0x13;
                System.arraycopy(longToBytes(pid), 0, dd, 4, 8);
                // Creating the datagram destination (that is the server socket address).
                final InetSocketAddress datagramDestination = new InetSocketAddress(serverAddress, serverPort);
                try {
                    DatagramPacket dp = new DatagramPacket(dd, 0, dd.length, datagramDestination);
                    socket.send(dp);
                } catch (Throwable ex) {
                    System.err.println("Not registered in central server.");
                    throw new RuntimeException(ex);
                }
                // Obtain and open the line.
                final SourceDataLine sLine;
                try {
                    sLine = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, getAudioFormat()));
                    sLine.open(getAudioFormat());
                } catch (LineUnavailableException ex) {
                    System.err.println("No S LINE");
                    throw new RuntimeException(ex);
                }

                // read chunks from a stream and write them to a source data
                sLine.start();
                final byte[] buffer = new byte[sLine.getBufferSize()];
                while (!Thread.interrupted()) {
                    //Receive request from client
                    final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    try {
                        socket.receive(packet);
                    } catch (IOException ex) {
                        if (!Thread.interrupted()) {
                            throw new RuntimeException("Cant get UDP message. " + ex.getMessage());
                        }
                    }
                    if (packet.getLength() != 0) {
                        sLine.write(buffer, 0, packet.getLength());
                    }
                }
            });
            receiveThread.setDaemon(true);
            receiveThread.start();
        }
        if (port > 0) {
            soundServerThread = new Thread(new Runnable() {
                private DatagramSocket socketIn;

                @Override
                public void run() {

                    // Obtain and open the line.
                    final SourceDataLine sLine;
                    try {
                        sLine = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, getAudioFormat()));
                        sLine.open(getAudioFormat());
                    } catch (LineUnavailableException ex) {
                        System.err.println("No S LINE");
                        throw new RuntimeException(ex);
                    }

                    // read chunks from a stream and write them to a source data 
                    sLine.start();

                    final byte[] buffer = new byte[sLine.getBufferSize()];
                    try {
                        socketIn = new DatagramSocket(port);
                    } catch (SocketException ex) {
                        throw new RuntimeException("Inpossible create socket on port " + port + ". " + ex.getMessage());
                    }
                    while (!Thread.interrupted()) {
                        //Receive request from client
                        final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                        try {
                            socketIn.receive(packet);
                        } catch (IOException ex) {
                            if (!Thread.interrupted()) {
                                throw new RuntimeException("Cant get UDP message. " + ex.getMessage());
                            }
                        }
                        if (packet.getLength() != 0) {
                            sLine.write(buffer, 0, packet.getLength());
                        }
                    }
                }
            });
            soundServerThread.setDaemon(true);
            soundServerThread.start();
        }
    }

    public void stopSoundServer() {
        soundServerThread.interrupt();
    }

    private Thread getSoundThread;

    /**
     * @param audioFormat nullable
     * @param toAudioFile nullable
     * @param inetAddress nullable
     * @param port        send to with port
     */
    public void open(final AudioFormat audioFormat, final File toAudioFile, final InetAddress inetAddress, final int port) {
        open(audioFormat, toAudioFile, inetAddress, port, null, null);
    }

    /**
     * @param audioFormat nullable
     * @param toAudioFile nullable
     * @param inetAddress nullable
     * @param port        send to with port
     * @param from        ID nullable
     * @param to          ID nullable
     */
    public void open(final AudioFormat audioFormat, final File toAudioFile, final InetAddress inetAddress, final int port, Long from, Long to) {
        stopThread(getSoundThread);

        getSoundThread = new Thread(() -> {
            if (audioFormat != null) {
                setAudioFormat(audioFormat);
            }
            TargetDataLine line = null;
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, getAudioFormat()); // format is an AudioFormat object
            if (!AudioSystem.isLineSupported(info)) {
                // Handle the error ...
                System.err.println("Audio System is NOT Line Supported");
                throw new RuntimeException("Audio System is NOT Line Supported");
            }
            // Obtain and open the line.
            try {
                line = (TargetDataLine) AudioSystem.getLine(info);
                line.open(getAudioFormat());
            } catch (LineUnavailableException ex) {
                System.err.println("No LINE");
                throw new RuntimeException(ex);
            }

            // Assume that the TargetDataLine, line, has already
            // been obtained and opened.
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            int numBytesRead;
            byte[] data = new byte[line.getBufferSize()];

            // Begin audio capture.
            line.start();

            stopped = false;
            // Here, stopped is a global boolean set by another thread.
            while (!Thread.interrupted()) {
                if (stopped) {
                    line.stop();
                    line.close();
                    getSoundThread.interrupt();
                } else {
                }
                // Read the next chunk of data from the TargetDataLine.
                numBytesRead = line.read(data, 0, data.length);
                // Save this chunk of data.
                if (toAudioFile != null) {
                    out.write(data, 0, numBytesRead);
                }
                if (numBytesRead != 0 && (inetAddress != null && port > 0)) {
                    sendUDPMessage(data, inetAddress, port, from, to);
                }
            }
            if (!stopped) {
                line.stop();
                line.close();
                // Read the next chunk of data from the TargetDataLine.
                numBytesRead = line.read(data, 0, data.length);
                // Save this chunk of data.
                out.write(data, 0, numBytesRead);
                if (numBytesRead != 0 && (inetAddress != null && port > 0)) {
                    sendUDPMessage(data, inetAddress, port, from, to);
                }
            }

            if (toAudioFile != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }

                final byte[] bb = out.toByteArray();
                final ByteArrayInputStream in = new ByteArrayInputStream(bb);
                try {
                    AudioSystem.write(
                            new AudioInputStream(in, getAudioFormat(), bb.length),
                            AudioFileFormat.Type.WAVE,
                            toAudioFile);
                    in.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        });
        getSoundThread.setDaemon(true);
        getSoundThread.start();
    }

    private volatile boolean stopped = false;

    public void stop() {
        stopped = true;
    }

    //**********************************************************************************************************************************************************
    //**********************************************************************************************************************************************************
    //**********************************************************************************************************************************************************
    private Thread transitServerThread;

    private final HashMap<Long, DatagramPacket> partners = new HashMap<>();

    public void startTransitServer(final int port) {
        stopThread(transitServerThread);

        transitServerThread = new Thread(new Runnable() {
            private MulticastSocket socket;

            @Override
            public void run() {

                System.out.println("Start transit");
                final byte[] buffer = new byte[1024 + 16 + 4];
                try {
                    socket = new MulticastSocket(port);
                    InetAddress group = InetAddress.getByName("230.0.0.1");
                    socket.joinGroup(group);
                } catch (IOException ex) {
                    throw new RuntimeException("Inpossible create socket on port " + port + ". " + ex.getMessage());
                }

                while (!Thread.interrupted()) {
                    //Receive request from client
                    final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    try {
                        socket.receive(packet);
                    } catch (IOException ex) {
                        if (!Thread.interrupted()) {
                            throw new RuntimeException("Cant get UDP message. " + ex.getMessage());
                        }
                    }

                    //String received = new String(packet.getData());
                    //System.out.println("Quote of the Moment: " + received);

                    byte[] in = packet.getData();

                    if (packet.getLength() == 8 + 4 && in[0] == 0x13 && in[1] == 0x13 && in[2] == 0x13 && in[3] == 0x13) {
                        //System.out.println("Connect from: " + getLong(in, 4));
                        partners.put(getLong(in, 4), packet);
                        continue;
                    }

                    if (packet.getLength() > 16 + 4 && in[0] == 0x13 && in[1] == 0x13 && in[2] == 0x13 && in[3] == 0x13) {
                        partners.put(getLong(in, 4), packet);
                        //System.out.println("MESS from: " + getLong(in, 4) + " to " + getLong(in, 12));
                        if (!partners.containsKey(getLong(in, 12))) {
                            continue;
                        }

                        final DatagramPacket packet2 = partners.get(getLong(in, 12));

                        packet2.setData(in, 16 + 4, packet.getLength() - (16 + 4));

                        try {
                            socket.send(packet2);
                        } catch (IOException ex) {
                            System.err.println("BAD @@@@@@@@@@@@@@@@@@");
                        }
                    }
                }
            }
        });
        transitServerThread.setDaemon(false);
        transitServerThread.start();
    }

    public static Long getLong(byte[] array, int offset) {
        return ((long) (array[offset] & 0xff) << 56)
                | ((long) (array[offset + 1] & 0xff) << 48)
                | ((long) (array[offset + 2] & 0xff) << 40)
                | ((long) (array[offset + 3] & 0xff) << 32)
                | ((long) (array[offset + 4] & 0xff) << 24)
                | ((long) (array[offset + 5] & 0xff) << 16)
                | ((long) (array[offset + 6] & 0xff) << 8)
                | ((long) (array[offset + 7] & 0xff));
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

}
