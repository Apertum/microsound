/*
 * This source code is under GPL license.
 */
package ru.apertum.microsound;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author evgeniy.egorov
 */
public class Stop {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception not success
     */
    public static void main(String[] args) throws Exception {
        System.out.println("Starting Black Elephant server...");
        try {
            final Socket socket = new Socket();
            try {
                System.out.println("Stopping server...");
                socket.connect(new InetSocketAddress("127.0.0.1", 38008));
            } catch (IOException ex) {
                throw new Exception("Fail to connect to server. ", ex);
            }
            try {
                final byte[] bb = new byte[3];
                bb[0] = 0x01;
                bb[1] = 0x02;
                bb[2] = 0x03;
                socket.getOutputStream().write(bb);
                socket.getOutputStream().flush();
                socket.getOutputStream().close();
                System.out.println("Server must be stopped.");
            } finally {
                socket.close();
            }
        } catch (IOException ex) {
            throw new Exception("Unable to get a response from the server. ", ex);
        }
    }

}
