/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

/**
 * @author Evgeniy Egorov
 */
public class Partner {
    String name;
    String address;
    int port;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public Partner(String name, String address, int port) {
        this.name = name;
        this.address = address;
        this.port = port;
    }

    @Override
    public String toString() {
        return (name + ". " + address + ":" + port).replaceAll("&", "");
    }

}
