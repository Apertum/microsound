/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author Evgeniy Egorov
 */
public class Signaller extends Application {

    public final ResourceBundle label = ResourceBundle.getBundle("ru/apertum/microsound/resources/label");

    public String l(String key) {
        return label.getString(key);
    }

    public static Image ICON;

    static {
        try {
            ICON = ImageIO.read(Signaller.class.getResource("/ru/apertum/microsound/resources/icon16.png"));
        } catch (IOException ex) {
        }
    }

    private QTray tray;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Params.get().curerntLocale);
        launch(args);
    }

    private Stage primaryStage;
    public static Signaller signaller = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        signaller = this;
        Platform.setImplicitExit(false);
        new Config().prepare(primaryStage);

        if (Params.get().load()) {
            primaryStage.show();
        }

        if (Params.get().cbReceiveMess) {
            System.out.println("Start Microsound receiver on port " + Params.get().editLocalPort);
            if (Params.get().cbUseServer) {
                Microsound.getInstance().startSoundServer(Params.get().editLocalPort, InetAddress.getByName(Params.get().editServerAddress), Params.get().editServerPort, Params.get().editID);
            } else {
                Microsound.getInstance().startSoundServer(Params.get().editLocalPort);
            }
        }

        Properties p = new Properties();
        p.load(Signaller.class.getResourceAsStream("/ru/apertum/microsound/microsound.properties"));
        JFrame marker = new JFrame("Microsound. ver." + p.getProperty("version") + "_" + p.getProperty("date"));
        marker.setIconImage(ICON);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        marker.setBounds(screenSize.width - 560, screenSize.height - 560, 560, 530);
        //marker.setUndecorated(true);
        marker.setAlwaysOnTop(true);
        marker.setLayout(new GridLayout(1, 1));
        marker.setForeground(java.awt.Color.YELLOW);
        marker.setBackground(java.awt.Color.YELLOW);
        JLabel lab1 = new JLabel();
        lab1.setOpaque(true);
        lab1.setForeground(java.awt.Color.YELLOW);
        lab1.setBackground(java.awt.Color.YELLOW);
        try {
            lab1.setIcon(new ImageIcon(ImageIO.read(PTTEmbedded.class.getResource("/ru/apertum/microsound/resources/icon512.png"))));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        lab1.setText("");
        marker.add(lab1);

        tray = QTray.getInstance(marker, "/ru/apertum/microsound/resources/icon16.png", "Microsound. Signaller");
        remakeMenu();
        this.primaryStage = primaryStage;
    }

    public void remakeMenu() {
        tray.removeItems();
        Params.get().list.forEach(p -> {
            tray.addItem(p.toString(), new CallEvent(p));
        });

        tray.addItem(l("Config"), (ActionEvent e) -> {
            Platform.runLater(() -> {
                primaryStage.show();
            });
        });

        tray.addItem(l("Exit"), (ActionEvent e) -> {
            System.exit(0);
        });
    }

    public class CallEvent implements ActionListener {

        private final Partner partner;
        private final Dialog<Partner> dialog = new Dialog<>();

        public CallEvent(Partner partner) {
            this.partner = partner;
            ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(ru.apertum.microsound.Config.ICON);
            dialog.setTitle("Microsound. Push to talk");
            dialog.setHeaderText(partner.toString());

            // Set the icon (must be included in the project).
            dialog.setGraphic(new ImageView(this.getClass().getResource("/ru/apertum/microsound/resources/icon48.png").toString()));

            // Set the button types.
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);

            // Create the username and password labels and fields.
            final GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            final Button btnPtt = new Button("", new ImageView(this.getClass().getResource("/ru/apertum/microsound/resources/icon48.png").toString()));
            btnPtt.setStyle("-fx-base: lightgreen;");
            btnPtt.setOnMousePressed((MouseEvent event) -> {
                try {
                    System.out.println("Open microthon input for destinaation: " + partner.address + ":" + partner.port);
                    Long id = null;
                    try {
                        id = Long.parseLong(partner.address);
                    } catch (NumberFormatException ex) {
                        id = null;
                    }
                    Microsound.getInstance().open(null, null, InetAddress.getByName(id == null ? partner.address : Params.get().editServerAddress),
                            id == null ? partner.port : Params.get().editServerPort, Params.get().editID, id);
                } catch (UnknownHostException ex) {
                    System.err.println(ex);
                }
                btnPtt.setEffect(new DropShadow(140, Color.YELLOW));
            });
            btnPtt.setOnMouseReleased((MouseEvent event) -> {
                System.out.println("Close microthon input");
                Microsound.getInstance().stop();
                btnPtt.setEffect(null);
            });

            grid.add(new Label(String.format(l("push_hint"), "\n", "\n")), 0, 0);
            grid.add(btnPtt, 1, 0);

            dialog.getDialogPane().setContent(grid);

            // Request focus on the username field by default.
            Platform.runLater(() -> btnPtt.requestFocus());

        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Platform.runLater(() -> {

                // Create the custom dialog.
                final Optional<Partner> result = dialog.showAndWait();
            });
        }

    }

}
