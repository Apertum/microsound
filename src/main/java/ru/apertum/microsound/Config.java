/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * @author Evgeniy Egorov
 */
public class Config extends Application {

    public static final Image ICON = new Image("/ru/apertum/microsound/resources/icon16.png");

    @Override
    public void start(Stage primaryStage) throws IOException {
        prepare(primaryStage);
        //this makes all stages close and the app exit when the main stage is closed
        //primaryStage.setOnCloseRequest(e -> Platform.exit());
        primaryStage.show();
    }

    public void prepare(Stage primaryStage) throws IOException {
        primaryStage.getIcons().add(ICON);
        Locale.setDefault(Params.get().curerntLocale);
        BorderPane root = FXMLLoader.load(getClass().getResource("FConfig.fxml"), ResourceBundle.getBundle("ru/apertum/microsound/resources/label", Params.get().curerntLocale));
        Scene scene = new Scene(root, 400, 800);
        primaryStage.setTitle("Microsound. Configuration");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(350);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
