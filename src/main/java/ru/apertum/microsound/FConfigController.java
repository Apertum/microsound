/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.microsound;

import java.net.URL;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import static ru.apertum.microsound.Config.ICON;

import ru.apertum.microsound.fx.PortField;

/**
 * FXML Controller class
 *
 * @author Evgeniy Egorov
 */
public class FConfigController implements Initializable {

    public final ResourceBundle label = ResourceBundle.getBundle("ru/apertum/microsound/resources/label");

    public String l(String key) {
        return label.getString(key);
    }

    /**
     * Initializes the controller class.
     *
     * @param url some url
     * @param rb  some resource
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lngLab.setGraphic(new ImageView(this.getClass().getResource("/ru/apertum/microsound/resources/flags/" + rb.getLocale().getLanguage() + "_" + rb.getLocale().getCountry() + ".png").toString()));
        setLocaleCombo();
        final ContextMenu contextMenuList = new ContextMenu();
        MenuItem addPartner = new MenuItem(l("add"));
        MenuItem editPartner = new MenuItem(l("edit"));
        MenuItem removePartner = new MenuItem(l("remove"));
        contextMenuList.getItems().addAll(addPartner, editPartner, removePartner);
        addPartner.setOnAction((ActionEvent event) -> {
            // Create the custom dialog.
            Dialog<Partner> dialog = new Dialog<>();
            ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(ICON);
            dialog.setTitle(l("add_new_partner"));
            dialog.setHeaderText(l("header1"));

            // Set the icon (must be included in the project).
            dialog.setGraphic(new ImageView(this.getClass().getResource("/ru/apertum/microsound/resources/icon48.png").toString()));

            // Set the button types.
            ButtonType loginButtonType = new ButtonType(l("add"), ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            // Create the username and password labels and fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            TextField username = new TextField();
            username.setPromptText(l("name"));
            PortField port = new PortField();
            TextField address = new TextField();
            address.setPromptText(l("address"));

            grid.add(new Label(l("name") + ":"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(new Label(l("address") + ":"), 0, 1);
            grid.add(address, 1, 1);
            grid.add(new Label(l("port") + ":"), 0, 2);
            grid.add(port, 1, 2);

            // Enable/Disable login button depending on whether a username was entered.
            Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
            loginButton.setDisable(true);

            // Do some validation (using the Java 8 lambda syntax).
            username.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });

            dialog.getDialogPane().setContent(grid);

            // Request focus on the username field by default.
            Platform.runLater(() -> username.requestFocus());

            // Convert the result to a username-password-pair when the login button is clicked.
            dialog.setResultConverter(dialogButton -> {
                if (dialogButton == loginButtonType) {
                    return new Partner(username.getText().trim(), address.getText().trim(), Integer.parseInt(port.getText()));
                }
                return null;
            });

            final Optional<Partner> result = dialog.showAndWait();

            result.ifPresent(partner -> {
                list.getItems().add(partner);
            });
        });

        edit = new EventHandler() {

            @Override
            public void handle(Event event) {

                if (list.getSelectionModel().getSelectedItems().size() != 0) {

                    // Create the custom dialog.
                    Dialog<Partner> dialog = new Dialog<>();
                    ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(ICON);
                    dialog.setTitle(l("edit_partner"));
                    dialog.setHeaderText(l("be_careful_address_port"));

                    // Set the icon (must be included in the project).
                    dialog.setGraphic(new ImageView(this.getClass().getResource("/ru/apertum/microsound/resources/icon48.png").toString()));

                    // Set the button types.
                    ButtonType loginButtonType = new ButtonType(l("edit"), ButtonData.OK_DONE);
                    dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

                    // Create the username and password labels and fields.
                    GridPane grid = new GridPane();
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(20, 150, 10, 10));

                    TextField username = new TextField(list.getSelectionModel().getSelectedItems().get(0).name);
                    username.setPromptText(l("name"));
                    PortField port = new PortField("" + list.getSelectionModel().getSelectedItems().get(0).port);
                    TextField address = new TextField(list.getSelectionModel().getSelectedItems().get(0).address);
                    address.setPromptText(l("address"));

                    grid.add(new Label(l("name") + ":"), 0, 0);
                    grid.add(username, 1, 0);
                    grid.add(new Label(l("address") + ":"), 0, 1);
                    grid.add(address, 1, 1);
                    grid.add(new Label(l("port") + ":"), 0, 2);
                    grid.add(port, 1, 2);

                    // Enable/Disable login button depending on whether a username was entered.
                    Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
                    //loginButton.setDisable(true);

                    // Do some validation (using the Java 8 lambda syntax).
                    username.textProperty().addListener((observable, oldValue, newValue) -> {
                        loginButton.setDisable(newValue.trim().isEmpty());
                    });

                    dialog.getDialogPane().setContent(grid);

                    // Request focus on the username field by default.
                    Platform.runLater(() -> username.requestFocus());

                    // Convert the result to a username-password-pair when the login button is clicked.
                    dialog.setResultConverter(dialogButton -> {
                        if (dialogButton == loginButtonType) {
                            return new Partner(username.getText(), address.getText(), Integer.parseInt(port.getText()));
                        }
                        return null;
                    });

                    final Optional<Partner> result = dialog.showAndWait();

                    result.ifPresent(partner -> {
                        list.getSelectionModel().getSelectedItems().get(0).address = partner.address.trim();
                        list.getSelectionModel().getSelectedItems().get(0).name = partner.name.trim();
                        list.getSelectionModel().getSelectedItems().get(0).port = partner.port;
                        list.refresh();
                    });

                }
            }

        };
        editPartner.setOnAction(edit);

        removePartner.setOnAction((ActionEvent event) -> {
            if (list.getSelectionModel().getSelectedItems().size() != 0) {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(ICON);
                alert.setTitle(l("removing_partner"));
                alert.setHeaderText(list.getSelectionModel().getSelectedItems().get(0).toString());
                alert.setContentText(l("refly_remove_this"));

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    list.getItems().remove(list.getSelectionModel().getSelectedItems().get(0));
                } else {
                    // ... user chose CANCEL or closed the dialog
                }

            }
        });

        list.setContextMenu(contextMenuList);
        Params.get().load();
        cbReceiveMess.setSelected(Params.get().cbReceiveMess);
        cbUseServer.setSelected(Params.get().cbUseServer);
        editServerAddress.setText(Params.get().editServerAddress);
        editServerPort.setText(Integer.toString(Params.get().editServerPort));
        editID.setText(Long.toString(Params.get().editID));
        cbNotify.setSelected(Params.get().cbNotify);
        editLocalPort.setText(Integer.toString(Params.get().editLocalPort));
        list.getItems().addAll(Params.get().list);
    }

    EventHandler<ActionEvent> edit;

    @FXML
    private void onCancel(ActionEvent event) {
        cbReceiveMess.getScene().getWindow().hide();
    }

    @FXML
    private void onSave(ActionEvent event) {
        Params.get().cbReceiveMess = cbReceiveMess.isSelected();
        Params.get().cbUseServer = cbUseServer.isSelected();
        Params.get().editServerAddress = editServerAddress.getText().trim();
        Params.get().editServerPort = Integer.parseInt(editServerPort.getText());
        Params.get().editID = Long.parseLong(editID.getText());
        Params.get().cbNotify = cbNotify.isSelected();
        Params.get().editLocalPort = Integer.parseInt(editLocalPort.getText());
        Params.get().list.clear();
        Params.get().list.addAll(list.getItems());
        Params.get().curerntLocale = getLocalizationInfo();
        Params.get().save();
        if (Signaller.signaller != null) {
            Signaller.signaller.remakeMenu();
        }
        final Alert alert = new Alert(AlertType.INFORMATION);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(ICON);
        alert.setTitle(l("saving"));
        alert.setHeaderText(null);
        alert.setContentText(l("saved_success"));
        alert.showAndWait();
    }

    @FXML
    private void onClickList(MouseEvent event) {
        if (event.getClickCount() > 1) {
            edit.handle(null);
        }
    }

    private void setLocaleCombo() {

        StringConverter<Locale> converter = new StringConverter<Locale>() {
            @Override
            public String toString(Locale object) {
                return String.format("%s (%s)", object.getDisplayLanguage(object), object.getDisplayCountry(object));
            }

            @Override
            public Locale fromString(String string) {
                return null;
            }
        };
        localeCombo.getItems().addAll(Params.LOCALES);
        localeCombo.setCellFactory(ComboBoxListCell.<Locale>forListView(converter));
        localeCombo.getSelectionModel().select(Params.get().curerntLocale);
    }

    public Locale getLocalizationInfo() {
        return localeCombo.getSelectionModel().getSelectedItem();
    }

    @FXML
    private CheckBox cbReceiveMess;
    @FXML
    private ListView<Partner> list;
    @FXML
    private CheckBox cbUseServer;
    @FXML
    private PortField editLocalPort;
    @FXML
    private CheckBox cbNotify;
    @FXML
    private TextField editServerAddress;
    @FXML
    private PortField editServerPort;
    @FXML
    private PortField editID;
    @FXML
    private ComboBox<Locale> localeCombo;
    @FXML
    private Label lngLab;

}
