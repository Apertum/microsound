/*
 * Copyright (C) 2014 Evgeniy Egorov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.microsound;

import ru.apertum.qsystem.client.forms.FClient;
import ru.apertum.qsystem.client.forms.FReception;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.cmd.RpcGetSelfSituation;
import ru.apertum.qsystem.common.model.INetProperty;
import ru.apertum.qsystem.extra.IStartClient;
import ru.apertum.qsystem.extra.IStartReception;
import ru.apertum.qsystem.extra.IStartServer;
import ru.apertum.qsystem.server.ServerProps;
import ru.apertum.qsystem.server.model.QUser;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * @author Evgeniy Egorov
 */
public class PTTEmbedded implements IStartClient, IStartReception, IStartServer {

    private void start(JButton btnPushToTalk, INetProperty netProperty) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        marker.setBounds(screenSize.width - 160, screenSize.height - 160, 128, 128);
        marker.setUndecorated(true);
        marker.setAlwaysOnTop(true);
        marker.setLayout(new GridLayout(1, 1));
        marker.setForeground(Color.YELLOW);
        marker.setBackground(Color.YELLOW);
        JLabel label = new JLabel();
        label.setOpaque(true);
        label.setForeground(Color.YELLOW);
        label.setBackground(Color.YELLOW);
        try {
            label.setIcon(new ImageIcon(ImageIO.read(PTTEmbedded.class.getResource("/ru/apertum/microsound/resources/icon128.png"))));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        label.setText("");
        marker.add(label);

        final java.awt.event.MouseAdapter ma = new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                QLog.l().logger().trace("Open microthon input for destinaation: " + netProperty.getAddress().getCanonicalHostName() + ":" + netProperty.getPort());
                Microsound.getInstance().open(null, null, netProperty.getAddress(), netProperty.getPort());
                marker.setVisible(true);
            }

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                QLog.l().logger().trace("Close microthon input. doublecheck");
                Microsound.getInstance().stop();
                marker.setVisible(false);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                QLog.l().logger().trace("Close microthon input");
                Microsound.getInstance().stop();
                marker.setVisible(false);
            }
        };

        try {
            btnPushToTalk.setIcon(new ImageIcon(ImageIO.read(PTTEmbedded.class.getResource("/ru/apertum/microsound/resources/icon48.png"))));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        btnPushToTalk.setText("");
        btnPushToTalk.setVisible(true);
        btnPushToTalk.addMouseListener(ma);
    }

    private final JFrame marker = new JFrame();

    @Override
    public String getDescription() {
        return "Embedded mode of Microsound for Push-To-Talk .";
    }

    @Override
    public long getUID() {
        return 17L;
    }

    @Override
    public void start(FClient form) {
        start(form.btnPushToTalk, form.getNetProperty());
    }

    @Override
    public void beforePressButton(QUser qUser, INetProperty iNetProperty, RpcGetSelfSituation.SelfSituation selfSituation, ActionEvent actionEvent, int i) {

    }

    @Override
    public void pressedButton(QUser qUser, INetProperty iNetProperty, RpcGetSelfSituation.SelfSituation selfSituation, ActionEvent actionEvent, int i) {

    }

    @Override
    public void beforePressButtonParallel(QUser qUser, INetProperty iNetProperty, RpcGetSelfSituation.SelfSituation selfSituation, ActionEvent actionEvent, int i) {

    }

    @Override
    public void pressedButtonParallel(QUser qUser, INetProperty iNetProperty, RpcGetSelfSituation.SelfSituation selfSituation, ActionEvent actionEvent, int i) {

    }

    @Override
    public void start(FReception form) {
        start(form.btnPushToTalk, form.getNetProperty());
    }

    @Override
    public void start() {
        QLog.l().logger().info("Start Microsound receiver on port " + ServerProps.getInstance().getProps().getServerPort());
        Microsound.getInstance().startSoundServer(ServerProps.getInstance().getProps().getServerPort());
    }
}
