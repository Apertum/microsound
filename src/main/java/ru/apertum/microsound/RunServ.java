package ru.apertum.microsound;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Отладка
 *
 * @author Evgeniy Egorov
 */
public class RunServ {

    public static void main(String[] args) throws InterruptedException, UnknownHostException {

        boolean first = true;
        //first = false;
        Long id = first ? 11111L : 22222L;
        System.out.println("client->");
        Microsound.getInstance().startSoundServer(InetAddress.getByName("127.0.0.1"), 11111, id);
        if (id == 22222L) {
            Microsound.getInstance().open(null, null, InetAddress.getByName("127.0.0.1"), 11111, 22222L, 11111L);
            Thread.sleep(500000);
            Microsound.getInstance().stop();
        } else {
            Thread.sleep(505000);
        }

    }

}
